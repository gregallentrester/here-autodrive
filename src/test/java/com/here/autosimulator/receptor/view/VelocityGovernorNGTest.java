package com.here.autosimulator.receptor.view;

// import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author greg
 */
public class VelocityGovernorNGTest {

  public VelocityGovernorNGTest() {

  }

  @BeforeClass
  public static void setUpClass() throws Exception {

  }

  @AfterClass
  public static void tearDownClass() throws Exception {

  }

  @BeforeMethod
  public void setUpMethod() throws Exception {

  }

  @AfterMethod
  public void tearDownMethod() throws Exception {

  }

  /**
   * Test of values method, of class VelocityGovernorFacade.
   */
  @Test
  public void testValues() {
    System.out.println("values");
    VelocityGovernorFacade[] expResult = null;
    VelocityGovernorFacade[] result = VelocityGovernorFacade.values();
//    assertEquals(result, expResult);

//    fail("The test case is a prototype.");
  }

  /**
   * Test of valueOf method, of class VelocityGovernorFacade.
   */
  @Test
  public void testValueOf() {
    System.out.println("valueOf");
    String name = "";
    VelocityGovernorFacade expResult = null;
//    VelocityGovernorFacade result = VelocityGovernorFacade.valueOf(name);
//    assertEquals(result, expResult);

//  fail("The test case is a prototype.");
  }

  /**
   * Test of acceptSignal method, of class VelocityGovernorFacade.
   */
  @Test
  public void testAcceptSignal() {
    System.out.println("acceptSignal");
    String value = "";
    int expResult = 0;
    
    String signal = "2";
    String currentTransientModality = "SAFE";
    int currentTransientVelocity = 6;
    
    
    int result = VelocityGovernorFacade.acceptSignal(
        signal, currentTransientModality, currentTransientVelocity);
  //  assertEquals(result, expResult);

//    fail("The test case is a prototype.");
  }
}
