package com.here.autosimulator.receptor.rulesengine;

import static org.testng.Assert.*;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.here.autosimulator.receptor.rulesengine.events.EventCompensator;


/**
 *
 * @author greg
 */
public class RulesEngineValveNGTest {

  public RulesEngineValveNGTest() {

  }

  @BeforeClass
  public static void setUpClass() throws Exception {

  }

  @AfterClass
  public static void tearDownClass() throws Exception {

  }

  @BeforeMethod
  public void setUpMethod() throws Exception {

  }

  @AfterMethod
  public void tearDownMethod() throws Exception {

  }

  /**
   * Test of enforceMinimumVelocity method, of class RulesEngineValve.
   */
  @Test
  public void testEnforceMinimumVelocity(){ 
  
    EventCompensator compensator = null;
    int currentTransientVelocity = 20;
    String currentTransientModality = "SAFE";
      
    System.out.println("enforceMinimumVelocity");
    
    RulesEngineValve instance = new RulesEngineValve();
    int expResult = 0;
    // int result = instance.enforceMinimumVelocity(
    //                compensator,
    //                currentTransientVelocity, 
    //                currentTransientModality);
    //
    // assertEquals(result, expResult);
  }

  /**
   * Test of encounteredSpeedSign method, of class RulesEngineValve.
   */
  @Test
  public void testEncounteredSpeedSign() {

    System.out.println("encounteredSpeedSign");

    RulesEngineValve instance = new RulesEngineValve();

    int expResult = 0;    
    int indice =0;
    
    String currentTransientModality = "SAFE";
    int currentTransientVelocity =2;
      
    int result = instance.encounteredSpeedSign(
    indice, currentTransientModality, currentTransientVelocity);
    
    // assertEquals(result, expResult);
  }

  /**
   * Test of rejectInvalidSignals method, of class RulesEngineValve.
   */
  @Test
  public void testRejectInvalidSignals() {
    
    System.out.println("rejectInvalidSignals");
    
    RulesEngineValve instance = new RulesEngineValve();
    
    int expResult = 0;
    int currentTransientVelocity = 7;
    int result = instance.rejectInvalidSignals(currentTransientVelocity);
  
    // assertEquals(result, expResult);
  }
}
