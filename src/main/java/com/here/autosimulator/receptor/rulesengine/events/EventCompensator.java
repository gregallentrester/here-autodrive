package com.here.autosimulator.receptor.rulesengine.events;


/**
 * Compose Event-response velocity information; instances of this class are 
 * persisted in an instance of the <code>AbsractMap</code> interface:
 * 
 *    <code>ConcurrentHashMap</code>
 */
public final class EventCompensator {

  /**
   * Encompass Event-response velocity information.
   *
   * @param isActivatedVal  has the mapping been engaged
   * @param modeVal the value used to store the scalar value for the current mode
   * @param normVal the value used to store the scalar value for the NORMAL mode
   * @param sportVal the value used to store the scalar value for the SPORT mode
   * @param safeVal the value used to store the scalar value for the SAFE mode
   */
  public EventCompensator(
      boolean isActivatedVal, Object modeVal, 
      int normVal, int sportVal, int safeVal) {

    isActivated = isActivatedVal;
    modality = modeVal;
    normal = normVal;
    sport = sportVal;
    safe = safeVal;
  }

  private boolean isActivated = false;
  /**  @return whether the mapping been engaged */
  public final boolean isActivated() { return isActivated; }
  /**  @return whether the mapping been engaged */
  public final boolean toggleActivated() { return isActivated ? false : true; }

  private Object modality = null;
  /**  @return the modality */
  public final Object getModality() { return modality; }

  private int normal = 0;
  /**  @return the modality value: NORMAL */
  public final int getNormal() { return normal; }

  private int sport = 0;
  /**  @return the modality value: SPORT */
  public final int getSport() { return sport; }

  private int safe = 0;
  /**  @return the modality value: SAFE */
  public final int getSafe() { return safe; }

  @Override
  public final String toString() {

    StringBuilder sb =
      new StringBuilder("isActivated ");

    sb.append(isActivated()).
    append(" | Modality ").append(getModality()).
    append(" | Normal ").append(getNormal()).
    append(" | Sport ").append(getSport()).
    append(" | Safe ").append(getSafe());

    return sb.toString();
  }

  /**
   * Create a copy for insertion (replaceement) into the map.
   *
   * @return a copy of the current instance of this class
   */
  public final EventCompensator copyInverted() {

    return new EventCompensator( ! isActivated, modality, normal, sport, safe);
  }
}
