package com.here.autosimulator.receptor.rulesengine;

import java.util.concurrent.ConcurrentHashMap;

import com.here.autosimulator.receptor.rulesengine.events.EventCompensator;

import com.here.autosimulator.receptor.rulesengine.modes.ValidModalities;


/**
 * Implements behavior for the <code>RulesEngine</code> interface which stiplulates
 * Velocity Policies to be imposed upon an instance of the <code>VelocityGovernor</code>.
 *
 * The <code>VelocityGovernor</code> class responds to signals from any client via
 * any client invoking this method/ask:
 *
 *   <blockquote>
 *      VelocityGovernor.acceptSignal(String signal);
 *   </blockquote>
 *
 * Implements the constraints listed next.
 *
 *  01 Traffic                  10-    05-    15-
 *  02 Traffic Clear			      10+    05+    15+
 *  03 Weather Rainy		        05-    05-    05-
 *  04 Weather Clear            05+    05+    05+
 *  05 Slippery Road		        15-    15-  	15-
 *  06 Slippery Road Clear      15+    15+    15+
 *  07 Emergency Turbo          20+  	 30+    10+
 *  10	Speed Limit Sign X      X  		 X+5    X-5
 *
 *  Rule 01 Set Initial speed to 20.
 *  Rule 02 For every valid sensor event, output the desired speed.
 *  Rule 03 Vehicle speed cannot fall below 10.
 *  Rule 04 Traffic Clear (02) cannot be applied if Traffic (01) doesn’t exist currently.
 *  Rule 05 Weather Clear (04) cannot be applied if Weather Rainy (03) doesn’t exist currently.
 *  Rule 06 Slippery Road Clear (06) cannot be applied if Slippery Road (05) doesn’t exist currently.
 *  Rule 07 Emergency Turbo (07) cannot be applied if it is Slippery Road (05).
 *  Rule 08 Emergency Turbo (07) cannot be applied more than once.
 *  Rule 09 A New Speed Limit Sign (10) clears Emergency Turbo (07).
 *  Rule 10 Any event number >= 10 is a Speed limit sign.
 *  Rule 11 Any event number > 100 is invalid.
 *  Rule 12 An event should be ignored if its already applied.
 *          Once cleared, an event can be re-applied.
 *          Assuming Normal mode, events 50, 1, 3, 1, 2, 1 result in 35 kph.
 *          The second Traffic event (01) was ignored,
 *          The third Traffic event (01) was applied (beause it was cleared).
 *  Rule 13 Ignore Valid events that cannot be applied because of other requirements.
 *  Rule 14 Print ‘invalid’ on invalid events, continue processing.
 */
public final class RulesEngineValve implements RulesEngine {

  /**
   * Rule 01:  Set Initial Velocity to 20.
   * Rule 02:  Implicit.
   */
  private static int BASE_VELOCITY = 20;
 
  /**
   * Any number of <code>static</code> methods  are allowed in a
   * Java 8 interface (and exactly one <code>default</code> method).
   *
   * @return the base velocity at inception of the application
   */
  public static int getBaseVelocity() { return BASE_VELOCITY; }
  /** 
   * @param value the integral, current base velocity
   * 
   * @see SensorHarness#beginSession(String positedSignal) 
   */
  public static void setBaseVelocity(int value) { BASE_VELOCITY = value; }
 
  private static final int VELOCITY_OUTLIER_LOWER_BOUNDS = 10;
  private static final int VELOCITY_UPPER_BOUNDS = 101;

  private static final int VELOCITY_LOWER_LIMIT = 0;
  private static final int VELOCITY_STATICALLY_MAPPED_UPPER_LIMIT = 8;
  private static final int VELOCITY_STATICALLY_MAPPED_UPPER_TOGGLE_LIMIT = 7;
  
  private static final int TURBO_INDICE = 7;
  private static boolean emergencyBoostExpended = false;
  
  private static final int SLIPPERY_ROAD_INDICE = 5;

  
  private static final ConcurrentHashMap model =
    new ConcurrentHashMap ();

  static {

      /* Ordinality, EventCompensator attrs: 
            isActivated, 
            modeDef, 
            normalMode Velocity, 
            sportMode Velocity, 
            safeMode Velocity */
      model.put(1, new EventCompensator(false, Events.TRAFFIC, -10, -05, -15));
      model.put(2, new EventCompensator(false, Events.TRAFFIC_CLEAR, 10, 05, 15));

      model.put(3, new EventCompensator(false, Events.WEATHER_RAINY, -05, -05, -05));
      model.put(4, new EventCompensator(false, Events.WEATHER_CLEAR, 05, 05, 05));

      model.put(5, new EventCompensator(false, Events.SLIPPERY_ROAD,  -15,  -15, -15));
      model.put(6, new EventCompensator(false, Events.SLIPPERY_ROAD_CLEAR, 15, 15, 15));

      model.put(7, new EventCompensator(false, Events.EMERGENCY_TURBO, 20, 30, 10));

      model.put(10, new EventCompensator(false, Events.SPEED_LIMIT_SIGN_X, 0,0,0));
  }

  /**
   * Convenience method performs a sanity check which verifies the existence of 
   * an instance of type:  <code>EventCompensator</code>; specifically, assuring 
   * that the indice maps to a valid member of an internal collection, of type:
   * <code>ConcurrentHashMap</code>.
   *
   * @param indice the index to a valid element in an internal collection 
   *               of <code>EventCompensator</code>
   *
   * @return the existence of the element, as dereferenced by the indice
   */
  public boolean containsIndice(int indice) {
    return model.keySet().contains(indice);
  }

  /**
   * Using the current state of the machine, and the posited delta
   * to that state-machine, calculate reasonableness of the delta by
   * applying the battery of navigation rules embodied in this class;
   * dispatch to the appropriate aggregate event-handler.
   *
   * @param indice
   * @param currentTransientModality
   * @param currentTransientVelocity
   * 
   * @return the new current velocity
   */
  public final int validate(
      int indice,
      String currentTransientModality,
      int currentTransientVelocity) {
   
    int answer = 0;

    // 10-100  
    if (indice >= VELOCITY_OUTLIER_LOWER_BOUNDS && 
        indice <= VELOCITY_UPPER_BOUNDS) {       
      
      answer = encounteredSpeedSign(
                  indice, currentTransientModality, currentTransientVelocity);
    }    
    //  apply statically-shaped compensation logic, 1-7
    else if (indice > VELOCITY_LOWER_LIMIT &&
        indice < VELOCITY_STATICALLY_MAPPED_UPPER_LIMIT) {

      // 1-6 the toggles
      if (indice < VELOCITY_STATICALLY_MAPPED_UPPER_TOGGLE_LIMIT) {

        answer = validateToggleLogic(
          indice, currentTransientModality, currentTransientVelocity);
      }
      /*  
       * Events that center on SlipperyRoad, EmergencyTurbo, SpeedLimitSign 
       *
       * Passive:
       * Event 05  Slippery Road         15-   15-   15-
       * Event 06  Slippery Road Clear   15+   15+   15+
       *
       * Active:
       * Event 07  Emergency Turbo       20+   30+   10+
       * Event 10  Speed Limit Sign X    X     X+5   X-5  
       * 
       * Applicable Rules:
       *
       * Rule 07  Emergency Turbo (07) not applied if Slippery Road (05) active.
       * Rule 08  Emergency Turbo (07) cannot be applied more than once.
       * Rule 09  A New Speed Limit Sign (10) clears Emergency Turbo (07).
       * Rule 10  Any event number >= 10 is a Speed limit sign.
       */ 
      else if (indice == TURBO_INDICE) {        
        
        int delta = 0;

        EventCompensator compensatorPrimary =
          (EventCompensator) model.get(TURBO_INDICE);
                
        EventCompensator compensatorSecondary =
          (EventCompensator) model.get(SLIPPERY_ROAD_INDICE);
    
        if ( ! compensatorSecondary.isActivated() &&
              emergencyBoostExpended == false) {

          EventCompensator replacement =
            compensatorPrimary.copyInverted();

          model.replace(TURBO_INDICE, replacement);
          emergencyBoostExpended = true;

          if (currentTransientModality.equalsIgnoreCase(
            ValidModalities.NORMAL.name())) {
            delta = compensatorPrimary.getNormal();
          }
          else if (currentTransientModality.equalsIgnoreCase(
            ValidModalities.SPORT.name())) {
            delta = compensatorPrimary.getSport();
          } 
          else if (currentTransientModality.equalsIgnoreCase(
            ValidModalities.SAFE.name())) {
            delta = compensatorPrimary.getSafe();
          }
        }
        
        answer = currentTransientVelocity + delta;
      }    
    }
    /*
     * Velocity-delta ask >= 10 <= 100, which equates to:
     *
     *    Events.SPEED_LIMIT_SIGN_X
     * 
     *  Semantics are:  Normal:  X | Sport: X+5 | Safe: X-5
     *
     *  Applicable Rules:
     *
     * Rule 09:  A New Speed Limit Sign (10) clears Emergency Turbo (event 07).
     * Rule 10:  Any event number >= 10 is a Speed limit sign.
     */
    // 10-101
   /// else if (indice >= VELOCITY_OUTLIER_LOWER_BOUNDS && 
   ///         indice <= VELOCITY_UPPER_BOUNDS) {

   ///   answer = currentTransientVelocity + indice;
  ///  }

    return answer;
  }

  /**
   * Rule 02:  Implicit.
   * Rule 03:  Velocity cannot fall below 10.
   *
   * @param compensator the compensatory velocity model (per modality) 
   * @param currentTransientVelocity the current velocity
   * @param currentTransientModality the currently-active modaity
   *
   * @return minimumVelocityBreached
   */
  public final int enforceMinimumVelocity(
      EventCompensator compensator,
      int currentTransientVelocity, 
      String currentTransientModality) {

    int delta = 0;

    if ( ! compensator.isActivated()) {

      if (currentTransientModality.equalsIgnoreCase(
        ValidModalities.NORMAL.name())) {
        delta = compensator.getNormal();
      }
      else if (currentTransientModality.equalsIgnoreCase(
        ValidModalities.SAFE.name())) {
        delta = compensator.getSafe();
      }
      else if (currentTransientModality.equalsIgnoreCase(
        ValidModalities.SPORT.name())) {
        delta = compensator.getSport();
      }

      if ((currentTransientVelocity + delta) >= 10) {

        return currentTransientVelocity + delta;
      }
    }

    return currentTransientVelocity;
  }

  /**
   * Rule 04 reads:
   * Traffic Clear (Event 02) cannot be applied
   * when Traffic (01) doesn’t exist (currently).
   *
   * Event  02  [Traffic Clear] Options:
   * Normal: +10  |  Sport: +05  |  Safe: +15
   *
   * @return trafficCautionClear - Adjudicated Velocity
   */
  public final int conditionallyClearTrafficCaution() {
    return 0;
  }

  /**
   * Event 03: [Weather Rainy] Options:
   * Normal: -05  |  Sport: -05  | 	Safe: -05
   *
   * @return weatherCaution - Adjudicated Velocity
   */
  public final int imposeWeatherCaution() {
    return 0;
  }

  /**
   * Rule 05 reads:
   * Weather Clear (event 04) cannot be applied if
   * Weather Rainy (event 03) doesn’t exist currently.
   *
   * Event 04: [Weather Clear] Options:
   * Normal: +05  |  Sport: +05  |	 Safe:  +05
   *
   * @return trafficCautionClear - Adjudicated Velocity
   */
  public final int conditionallyClearWeatherCaution() {
    return 0;
  }

  /**
   * Event 05: [Slippery Road]	Options:
   * Normal: -15  |  Sport: -15  |  Safe: -15
   *
   * @return slipperyRoad - Adjudicated Velocity
   */
  public final int imposeSlipperyRoadCaution() {
    return 0;
  }

  /**
   * Rule 06 reads:
   * Slippery Road Clear (event 06) cannot be applied if
   * Slippery Road (event 05) doesn’t exist (currently).
   *
   * Event 06 [Slippery Road Clear] Options:
   * Normal: +15  |  Sport: +15  |  +15
   *
   * @return slipperyRoadCautionClear - Adjudicated Velocity
   */
  public final int conditionallyClearSlipperyRoadCaution() {
    return 0;
  }

  /**
   * Rule 07 reads:
   * Emergency Turbo (event 07) cannot be
   * applied if exists Slippery Road (event 05).
   *
   * Rule 08 reads:
   * Emergency Turbo (event 07) cannot be applied more than once.
   *
   * @return applyEmergencyTurbo - Adjudicated Velocity
   *
   * @see #encounteredSpeedSign()
   */
  public final int conditionallyApplyEmergencyTurbo() {
    return 0;
  }

  /**
   * Rule 09 reads:
   * A New Speed Limit Sign (10) clears
   * Emergency Turbo (event 07)
   *
   * Event of a cardinality of 10 or more [Speed Limit Sign X] Options:
   * Normal: X | Sport: X+5 | Safe: X-5
   *
   * Applicable Events:
   * 
   * 08 Emergency Turbo (event 07) cannot be applied more than once
   * 09 A New Speed Limit Sign (10) clears Emergency Turbo (event 07)
   * 10 Any event number >= 10 is a Speed limit sign
   * 
   * 07 Emergency Turbo n: +20  +30  +10
   * 
   * @param indice the eventID that is a key to the internal map
   * @param currentTransientModality the current modality
   * @param currentTransientVelocity the current velocity
   * 
   * @return conditionallyApplyEmergencyTurbo - Adjudicated Velocity
   *
   * @see #conditionallyApplyEmergencyTurbo()
   */
  public final int encounteredSpeedSign(
      int indice, String currentTransientModality, 
      int currentTransientVelocity) {
            
    EventCompensator compensatorPrimary =
      (EventCompensator) model.get(TURBO_INDICE);
    
    if (compensatorPrimary.isActivated() &&
        emergencyBoostExpended == false) {

      EventCompensator replacement =
        compensatorPrimary.copyInverted();

      model.replace(TURBO_INDICE, replacement);
      emergencyBoostExpended = true;
    }
      
    return indice;   
  }

  /**
   * Rule 10 reads:
   * Any event number >= 10 is a Speed limit sign.
   * Rule 11 reads:
   * Any event number > 100 is invalid.
   *
   * @return conditionallyObserverSpeedSigns - Adjudicated Velocity
   */
  public final int conditionallyObserverSpeedSigns() {
    return 0;
  }

  /**
   * Rule 13 reads:
   * Ignore valid events which cannot be applied because of other rules.
   * Rule 14 reads:  Print ‘invalid’ on invalid events, continue to process.
   *
   * @param currentTransientVelocity the current velocity 
   * 
   * @return rejectInvalidSignals - Adjudicated Velocity as a cipher (a zero value)
   */
  public final int rejectInvalidSignals(int currentTransientVelocity) {
    System.err.println("Invalid");
    return currentTransientVelocity;
  }

  /**
   * Aggregated toggle logic; Applicable Rules:
   * 
   * 04. Traffic Clear (event 02) cannot be applied if 
   *     Traffic (event 01) doesn’t exist currently.
   * 
   * 05. Weather Clear (event 04) cannot be applied if 
   *     Weather Rainy (event 03) doesn’t exist currently.
   * 
   * 06. Slippery Road Clear (event 06) cannot be applied if 
   *     Slippery Road (event 05) doesn’t exist currently.
   * 
   * @param indice integeral rule value dereferences applicable
   *               <code>EventCompensator</code>.
   * 
   * @param currentTransientModality  the current transient modality
   * @param currentTransientVelocity the current transient velocity
   * 
   * @return the adjudicated velocity
   * 
   * @see #enforceMinimumVelocity(EventCompensator compensator, int currentTransientVelocity, String currentTransientModality
   */
  public final int validateToggleLogic(
      int indice, String currentTransientModality, int currentTransientVelocity) {

    int answer = currentTransientVelocity;

    // Case: Odd Indice
    if (indice % 2 != 0) {

      EventCompensator compensator =
        (EventCompensator) model.get(indice);

      int temp = 
        enforceMinimumVelocity(
          compensator, 
          currentTransientVelocity, 
          currentTransientModality);

      if (temp != answer) {

        answer = temp;

        EventCompensator replacement =
          compensator.copyInverted();

        model.replace(indice, replacement);
      }

      return answer;
    }
    else if (indice % 2 == 0) { // Case: Even Indice 
      
      EventCompensator compensatorPrimary =
        (EventCompensator) model.get(indice);

      EventCompensator compensatorSecondary =
        (EventCompensator) model.get(indice-1);

      if (compensatorSecondary.isActivated()) {

        EventCompensator replacement =
          compensatorSecondary.copyInverted();
              
        model.replace(indice-1, replacement);
        
        int delta = 0;
        
        if (currentTransientModality.equalsIgnoreCase(
          ValidModalities.NORMAL.name())) {
          delta = compensatorPrimary.getNormal();
        }
        else if (currentTransientModality.equalsIgnoreCase(
          ValidModalities.SAFE.name())) {
          delta = compensatorPrimary.getSafe();
        }
        else if (currentTransientModality.equalsIgnoreCase(
          ValidModalities.SPORT.name())) {
          delta = compensatorPrimary.getSport();
        }

        return currentTransientVelocity + delta;
      }
    }

    return answer;
  }

  /**
   * Primarily used to debug, never exposed to end user; according
   * to the requirements, the end user must implicitly "know" the
   * lower-level semantics of the integral (opaque) values that they
   * submit as synthetic, console-based "Events".
   *
   * Enforces the legal values for Operational Events for use by an instance
   * of the class:
   * <code>RulesEngineCurator</code>
   *
   * which itself is encapsulated by an instance of the class:
   * <code>SensorHarness</code>.
   *
   * NB  Can become its own object.
   */
  public enum Events {

    /**  */
    TRAFFIC,

    /**  */
    TRAFFIC_CLEAR,

    /**  */
    WEATHER_RAINY,

    /**  */
    WEATHER_CLEAR,

    /**  */
    SLIPPERY_ROAD,

    /**  */
    SLIPPERY_ROAD_CLEAR,

    /**  */
    EMERGENCY_TURBO,

    /**  */
    SPEED_LIMIT_SIGN_X
  }
}
