package com.here.autosimulator.receptor.rulesengine;

import com.here.autosimulator.receptor.rulesengine.events.EventCompensator;


/**
 *  * Implements behavior for the <code>RulesEngine</code> interface which stiplulates
 * Velocity Policies to be imposed upon an instance of the <code>VelocityGovernor</code>.
 *
 * The <code>VelocityGovernor</code> class responds to signals from any client via
 * any client invoking this method/ask:
 *
 *   <blockquote>
 *      VelocityGovernor.acceptSignal(String signal);
 *   </blockquote>
 *
 * Implements the constraints listed next.
 *
 *  01 Traffic                  10-    05-    15-
 *  02 Traffic Clear			      10+    05+    15+
 *  03 Weather Rainy		        05-    05-    05-
 *  04 Weather Clear            05+    05+    05+
 *  05 Slippery Road		        15-    15-  	15-
 *  06 Slippery Road Clear      15+    15+    15+
 *  07 Emergency Turbo          20+  	 30+    10+
 *  10	Speed Limit Sign X      X  		 X+5    X-5
 *
 *  Rule 01 Set Initial speed to 20.
 *  Rule 02 For every valid sensor event, output the desired speed.
 *  Rule 03 Vehicle speed cannot fall below 10.
 *  Rule 04 Traffic Clear (02) cannot be applied if Traffic (01) doesn’t exist currently.
 *  Rule 05 Weather Clear (04) cannot be applied if Weather Rainy (03) doesn’t exist currently.
 *  Rule 06 Slippery Road Clear (06) cannot be applied if Slippery Road (05) doesn’t exist currently.
 *  Rule 07 Emergency Turbo (07) cannot be applied if it is Slippery Road (05).
 *  Rule 08 Emergency Turbo (07) cannot be applied more than once.
 *  Rule 09 A New Speed Limit Sign (10) clears Emergency Turbo (07).
 *  Rule 10 Any event number >= 10 is a Speed limit sign.
 *  Rule 11 Any event number > 100 is invalid.
 *  Rule 12 An event should be ignored if its already applied.
 *          Once cleared, an event can be re-applied.
 *          Assuming Normal mode, events 50, 1, 3, 1, 2, 1 result in 35 kph.
 *          The second Traffic event (01) was ignored,
 *          The third Traffic event (01) was applied (beause it was cleared).
 *  Rule 13 Ignore Valid events that cannot be applied because of other requirements.
 *  Rule 14 Print ‘invalid’ on invalid events, continue processing.
 *
 * @see RulesEngineValve
 */
public interface RulesEngine {

  /**
   * Convenience method performs a sanity check which verifes the existence of 
   * an instance -- of type:  <code>EventCompensator</code>; specifically 
   * assuring that the indice maps to a valid member of an internal collection, 
   * of type:  <code>ConcurrentHashMap</code>.
   *
   * @param indice the index to a valid element in an internal 
   *               collection of <code>EventCompensator</code>
   *
   * @return the existence of the element, as dereferenced by the indice
   */
  public boolean containsIndice(int indice);


  /**
   * Using the current state of the machine, and the posited delta
   * to that state-machine, calculate reasonableness of the delta
   * by applying the battery of navigation rules embodied in a
   * concrete implementor of this interface.
   *
   * @param indice
   * @param currentTransientModality
   * @param currentTransientVelocity
   * 
   * @return the new current velocity
   */
  public abstract int validate(
      int indice,
      String currentTransientModality,
      int currentTransientVelocity);

  /**
   * Rule 02:  Implicit.
   * Rule 03:  Velocity cannot fall below 10.
   *
   * @param compensator the compensatory velocity model (per modality) 
   * @param currentTransientVelocity the current velocity
   * @param currentTransientModality the currently-active modaity
   * 
   * @return the adjudicated velocity
   */
  public abstract int enforceMinimumVelocity(
      EventCompensator compensator,
      int currentTransientVelocity, 
      String currentTransientModality);

  /**
   * Rule 04 reads:
   * Traffic Clear (Event 02) cannot be applied
   * when Traffic (01) doesn’t exist (currently).
   *
   * Event  02  [Traffic Clear] Options:
   * Normal: +10  |  Sport: +05  |  Safe: +15
   *
   * @return trafficCautionClear - Adjudicated Velocity
   */
  public abstract int conditionallyClearTrafficCaution();

  /**
   * Event 03: [Weather Rainy] Options:
   * Normal: -05  |  Sport: -05  | 	Safe: -05
   *
   * @return weatherCaution - Adjudicated Velocity
   */
  public abstract int imposeWeatherCaution();

  /**
   * Rule 05 reads:
   * Weather Clear (event 04) cannot be applied if
   * Weather Rainy (event 03) doesn’t exist currently.
   *
   * Event 04: [Weather Clear] Options:
   * Normal: +05  |  Sport: +05  |	 Safe:  +05
   *
   * @return trafficCautionClear - Adjudicated Velocity
   */
  public abstract int conditionallyClearWeatherCaution();

  /**
   * Event 05: [Slippery Road]	Options:
   * Normal: -15  |  Sport: -15  |  Safe: -15
   *
   * 
   * @return slipperyRoad - Adjudicated Velocity
   */
  public abstract int imposeSlipperyRoadCaution();

  /**
   * Rule 06 reads:
   * Slippery Road Clear (event 06) cannot be applied if
   * Slippery Road (event 05) doesn’t exist (currently).
   *
   * Event 06 [Slippery Road Clear] Options:
   * Normal: +15  |  Sport: +15  |  +15
   *
   * @return slipperyRoadCautionClear - Adjudicated Velocity
   */
  public abstract int conditionallyClearSlipperyRoadCaution();

  /**
   * Rule 07 reads:
   * Emergency Turbo (event 07) cannot be
   * applied if exists Slippery Road (event 05).
   *
   * Rule 08 reads:
   * Emergency Turbo (event 07) cannot be applied more than once.
   *
   * @return applyEmergencyTurbo - Adjudicated Velocity
   *
   * @see #encounteredSpeedSign()
   */
  public abstract int conditionallyApplyEmergencyTurbo();

  /**
   * Rule 09 reads:
   * A New Speed Limit Sign (10) clears
   * Emergency Turbo (event 07).
   *
   * Event of a cardinality of 10 or more [Speed Limit Sign X] Options:
   * Normal: X  |  Sport: X+5  |  Safe: X-5
   *
   * @param indice the eventID that is a key to the internal map
   * @param currentTransientModality the current modality
   * @param currentTransientVelocity the current velocity
   * 
   * @see #conditionallyApplyEmergencyTurbo()
   * 
   * @return conditionallyApplyEmergencyTurbo - Adjudicated Velocity
   *
   * @see #conditionallyApplyEmergencyTurbo()
   */
  public abstract int encounteredSpeedSign(
    int indice, String currentTransientModality, int currentTransientVelocity);

  /**
   * Rule 10 reads:
   * Any event number >= 10 is a Speed limit sign.
   * Rule 11 reads:
   * Any event number > 100 is invalid.
   *
   * @return conditionallyObserverSpeedSigns - Adjudicated Velocity
   */
  public abstract int conditionallyObserverSpeedSigns();

  /**
   * Rule 13 reads:
   * Ignore valid events which cannot be applied because of other rules.
   * Rule 14 reads:  Print ‘invalid’ on invalid events, continue to process.
   * 
   * @param currentTransientVelocity the current velocity 
   * @return rejectInvalidSignals - Adjudicated Velocity as a cipher (a zero value)
   */
  public abstract int rejectInvalidSignals(int currentTransientVelocity);

  /**
   * Aggregated toggle logic.
   *
   * @param indice integeral rule value dereferences 
   *               applicable <code>EventCompensator</code>.
   * @param currentTransientModality  the current transient modality
   * @param currentTransientVelocity the current transient velocity
   * 
   * @return  the adjudicated velocity
   */
  public abstract int validateToggleLogic(
      int indice, String currentTransientModality, int currentTransientVelocity);

}
