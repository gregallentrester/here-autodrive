package com.here.autosimulator.receptor.rulesengine.modes;

/**
 * Embodies the legal values for Operational Modalties - which are used by both  
 * an instance of the <code>RulesEngineValve</code> class, and an instance of 
 * the <code>SensorHarness</code> class.
 */
public enum ValidModalities {

  /** */
  NORMAL,

  /** */
  SPORT,

  /** */
  SAFE
}
