package com.here.autosimulator.receptor.dispatch;

import com.here.autosimulator.receptor.rulesengine.RulesEngine;
import com.here.autosimulator.receptor.rulesengine.RulesEngineValve;

/**
 * Accepts incoming signal (which is of type <code>String</code>); the signal 
 * signifies EITHER:  A value representing a state change to the Modality aspect
 * OR a scalar value that represents an EventID (resolves to an integral value)
 * that acts as a key to lookup velocity values stored in an Event Map.   
 */
public final class RulesEngineCurator {

  private static final int NO_VELOCITY_DELTA_SENT = 0;

  private static final int VELOCITY_STATICALLY_MAPPED_UPPER_LIMIT = 8;
  private static final int VELOCITY_OUTLIER_LOWER_BOUNDS = 10;
  private static final int VELOCITY_UPPER_BOUNDS = 101;

  private static final RulesEngine RULES_ENGINE =
    new RulesEngineValve();

  
  /**
   * Inform the <code>RulesEngineValve </code> of
   * the transient state-transition ask (the Client's ask).
   *
   * @see #acceptSignal(String modality, int acceptSignal)
   */
  private static int dispatchToRulesEngine(
      String signal, 
      String currentTransientModality,
      int currentTransientVelocity) {

    int indice = 0;
    int answer = NO_VELOCITY_DELTA_SENT;

    if ( ! signal.equalsIgnoreCase("SAFE") &&
         ! signal.equalsIgnoreCase("SPORT") &&
         ! signal.equalsIgnoreCase("NORMAL")) {

      indice = Integer.parseInt(signal);

      //  CASE:  indice < 8 OR  (indice >= 10 AND indice <= 100)
      if (indice < VELOCITY_STATICALLY_MAPPED_UPPER_LIMIT ||
          (indice >=  VELOCITY_OUTLIER_LOWER_BOUNDS  &&
          indice <= VELOCITY_UPPER_BOUNDS)) {

        answer =
          RULES_ENGINE.validate(
              indice, currentTransientModality, currentTransientVelocity);
      }
      // CASE: indice > 100 
      //       Passively ignore signal by short-circuiting processing 
      //       then return delta value that is the equivalent of zero 
      else if (indice >= VELOCITY_UPPER_BOUNDS) {
        answer = RULES_ENGINE.rejectInvalidSignals(currentTransientVelocity);
      }
    }
    else if (signal.equalsIgnoreCase("SAFE") ||
             signal.equalsIgnoreCase("SPORT") ||
             signal.equalsIgnoreCase("NORMAL")) {

      currentTransientModality = signal;
    }

    return answer;
  }

  /**
   * Pass-thru adapter method accepts signal information from the
   * <code>VelocityGovernor</code>(the View,) then delegates to
   * an internal RulesEngine-logic method.
   *
   * @param signal the incoming value: either a scalar velocity value
   *                         or a modality value as a <code>String</code>
   * 
   * @param currentTransientModality the current modality
   * 
   * @param currentTransientVelocity the current velocity
   * 
   * @return the adjudicated velocity
   *
   * @see VelocityGovernor#acceptSignal(String signal)
   */
  public synchronized static final int acceptSignal(
      String signal, 
      String currentTransientModality,
      int currentTransientVelocity) {

    return dispatchToRulesEngine(
        signal, currentTransientModality, currentTransientVelocity);
  }
}
