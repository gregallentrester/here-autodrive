package com.here.autosimulator.receptor.view;

import com.here.autosimulator.receptor.dispatch.RulesEngineCurator;

/**
 * Stateless conceptualiation of a traditional/mechanical Velocity Governor:   
 * Accepts sensor-harness signals, then relays the signal to an instance of
 * the <code>RulesEngineCurator</code> class, which delegates to an instance 
 * of <code>RulesEngineValve</code> class.
 */
public enum VelocityGovernorFacade {

  /** anything */ 
  ANY;

  /**
   * Pass-thru adapter method that accepts signal information from the
   * <code>SensorConsole</code> (the View), then delegates to a method: 
   * <code>RulesEngineCurator.acceptSignal(...)</code>
   *
   * @param signal the String value representing: 
   *               EITHER:  the desired scalar, integral velocity delta value, 
   *               OR:      a String value representing the desired Modality.
   *       
   * @param currentTransientModality the current modality
   * @param currentTransientVelocity the current velocity
   *
   * @return the resultant adjudicated current velocity
   * 
   * @see RulesEngineCurator#acceptSignal(int signal, int currentTransientVelocity)
   */
  public static final int acceptSignal(
      String signal, String currentTransientModality, 
      int currentTransientVelocity) {

    return RulesEngineCurator.acceptSignal(
          signal, currentTransientModality, currentTransientVelocity);
  }
}
