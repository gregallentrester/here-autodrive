package com.here.autosimulator.harness;

import java.util.Scanner;

import com.here.autosimulator.receptor.rulesengine.RulesEngineValve;
import com.here.autosimulator.receptor.view.VelocityGovernorFacade;
import com.here.autosimulator.receptor.rulesengine.modes.ValidModalities;


/**
 * Submits serial Velocity Vector Signals to an instance of the class:
 * <code>VelocityGovernorFacade</code>; it sends signals that originate
 * from conventional ßash console command-line.
 *
 * This class contains Lifecycle-logic/Presentation-logic that stipulates:
 *
 * A.  Valid values are either:
 * 
 *       - Numeric/scalar values  (EventIDs):  1-7, 10-100
 *       - String-base Modal Representations: Q/QUIT | SAFE | SPORT | NORMAL
 *
 * ß.  Invalid values that are echoed, but ignored
 *
 * All (intergral, scalar) ranges, bounds, indices have been declared 
 * static final; this avoids exhibiting the Magic Number Antipattern.
 *
 * NB  Circa: Line 138, the delegation to the VelocityGovernorFacade reference
 */
public final class SensorHarness {

  private static StringBuilder inceptionState =
    new StringBuilder("\n\nWelcome.\nMode is " );

  private static final int MANDATORY_ARG_COUNT = 1;
  private static final int ZERO_INDICE = 0;
  private static final int EXIT_OK = 0;

  /* invariant reference to a "mutable" memory block (via the 
     (canonical method <code>map.replace(key, instance)</code>  */
  private static final StringBuilder usageMessage =
    new StringBuilder("\n\nInput Error\n\n");

  private static final String COMMAND = "   ./autonomousDriving ";
  private static final String ACTUAL_INPUT = "\n\nActual: \n\n ";

  private static final String USAGE = "Usage: \n\n " + COMMAND;
  private static final String PROMPT = "\n\nEnter Event: ";

  private static final String PIPE = " | ";
  private static final String QUIT = "QUIT";
  private static final String Q = "Q";
  private static final String STOPPED = "\n\nSTOPPED ...\n\n ";
  private static final String EPILOGUE = "\n\t...";

  private static String signal = "";

  /** as per requirements, velocity starts @ 20 */
  private int currentTransientVelocity = 20;  


  /** Off-load some work from the instance-level 
      constuctor to this classs-level constructor */
  static {

    usageMessage.
      append(USAGE).
      append(ValidModalities.NORMAL.name()).append(PIPE).
      append(ValidModalities.SPORT.name()).append(PIPE).
      append(ValidModalities.SAFE.name()).append(EPILOGUE);
  }

  /**
   * Canonical entry point into a standalone Java application
   * 
   * Accept from the command-line, the value for the driving
   * mode as type <code>String</code> insist on it.
   *
   * @param args [] the incoming command-line values
   */
  public static void main(String args[]) {

    if (args.length == MANDATORY_ARG_COUNT) {
      signal = args[ZERO_INDICE].toUpperCase();
    }

    if (args.length != MANDATORY_ARG_COUNT ||
        ( ! signal.equalsIgnoreCase(ValidModalities.NORMAL.name()) &&
          ! signal.equalsIgnoreCase(ValidModalities.SPORT.name()) &&
          ! signal.equalsIgnoreCase(ValidModalities.SAFE.name()))) {

      System.err.println(
        usageMessage.
        append(ACTUAL_INPUT).
        append(COMMAND).
        append(signal).
        append(EPILOGUE).
        toString());

      System.exit(EXIT_OK);
    }

    new SensorHarness().beginSession(signal);
  }

  /**
   * Leverage <code>java.util.Scanner</code> to accept the next maneover.
   *
   * @param positedSignal the incoming signal
   */
  private void beginSession(String positedSignal) {

    /* "dramatic", one-time autoboxing which
       imposes no undue processing burden */
    boolean OK = Boolean.TRUE;

    System.err.println(
      inceptionState.
      append(positedSignal).
      append("\n").
      append(RulesEngineValve.getBaseVelocity()).  // static class method
      append(PROMPT).toString());

    Scanner scanner = new Scanner(System.in);

    try {  /* Client Lifecycle (try/finally) */

      String currentTransientModality = signal;

      while (OK) {

        positedSignal = scanner.nextLine();

        try {  /* try/catch (NFE) */

          if ( ! positedSignal.equalsIgnoreCase(QUIT) &&
               ! positedSignal.toUpperCase().startsWith(Q)) {

            Integer.parseInt(positedSignal);

            currentTransientVelocity = 
              VelocityGovernorFacade.acceptSignal(
                  positedSignal, 
                  currentTransientModality, 
                  currentTransientVelocity);

            System.err.println(currentTransientVelocity);

            RulesEngineValve.setBaseVelocity(currentTransientVelocity);

            System.err.println(PROMPT);
            OK = Boolean.TRUE;
          }
          else if (positedSignal.equalsIgnoreCase(QUIT) ||
                   positedSignal.toUpperCase().startsWith(Q)) {

            // dramatic, one-time autoboxing which
            // imposes no undue processing burden
            OK = Boolean.FALSE;
          }
        }
        catch (NumberFormatException e) {
          /*
           * Swallow the NFE, continue; this accomodates/ignores
           * bad, albeit benign, input.
           * 
           * NB  Antipattern citation:
           * Processing logic (or delegation to processing logic) does
           * not belong within an exception block.  A less performant 
           * alternative is to check each character -- up to the first 
           * fail, if any -- to see whether it is a digit (this path 
           * is a relatively highly-trafficked one).
           *
           * NB  Perform longitudinal testing to prove efficiency.
           */
          currentTransientModality = 
            handleNFEException(positedSignal);

          System.err.println(PROMPT);
          continue;
        }
      }
    }
    finally {
      scanner.close();
      
      System.err.println(STOPPED);
      System.exit(EXIT_OK);
    }
  }


  /**
   * Filter input from the end user (producer) to admit the following 
   * <code>String</code> values (and to provide an integration point:
   *
   *   Q/QUIT | SAFE | SPORT | NORMAL
   */
  private String handleNFEException(String incomingSignal) {

    if (incomingSignal.equalsIgnoreCase(ValidModalities.SAFE.name()) ||
        incomingSignal.equalsIgnoreCase(ValidModalities.SPORT.name()) ||
        incomingSignal.equalsIgnoreCase(ValidModalities.NORMAL.name())) {

      return incomingSignal;
    }

    return ValidModalities.SAFE.name();
  }
}
